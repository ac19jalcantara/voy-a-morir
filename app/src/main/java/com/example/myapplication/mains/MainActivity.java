package com.example.myapplication.mains;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.control.control;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button boton;
    EditText t1;
    private int mYearIni, mMonthIni, mDayIni, sYearIni, sMonthIni, sDayIni;
    static final int DATE_ID = 0;
    Calendar C = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boton=findViewById(R.id.idMuerte);
        boton.setOnClickListener(this);
        sMonthIni = C.get(Calendar.MONTH);
        sDayIni = C.get(Calendar.DAY_OF_MONTH);
        sYearIni = C.get(Calendar.YEAR);

        t1 = (EditText) findViewById(R.id.idFecha);

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(DATE_ID);
            }
        });

    }

    private void colocar_fecha() {
        t1.setText(mDayIni + "-" +mMonthIni+ "-" +mYearIni+" ");
    }



    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    mYearIni = year;
                    mMonthIni = monthOfYear;
                    mDayIni = dayOfMonth;
                    colocar_fecha();

                }

            };


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                return new DatePickerDialog(this, mDateSetListener, sYearIni, sMonthIni, sDayIni);


        }


        return null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.idMuerte){
                Intent i = new Intent (this,MainActivity2_muerte.class);

            EditText textNom = (EditText) findViewById(R.id.idName);
            EditText textLugar = (EditText) findViewById(R.id.idNacimiento);

            if(!control.editTextIsNullOrEmpty(textNom) && !control.editTextIsNullOrEmpty(textLugar)) {
                String fecha = t1.getText().toString();
                i.putExtra("date", fecha);

                String nom = textNom.getText().toString();
                i.putExtra("name", nom);

                String lugar = textLugar.getText().toString();
                i.putExtra("place", lugar);

                Spinner sSexo = (Spinner) findViewById(R.id.idSpinner);
                String sexo = sSexo.getSelectedItem().toString();
                i.putExtra("gender", sexo);


                CheckBox cFumador = (CheckBox) findViewById(R.id.idFumador);
                boolean fumador = cFumador.isChecked();
                i.putExtra("smoker", fumador);

                CheckBox cDeportista = (CheckBox) findViewById(R.id.idDeportista);
                boolean deportista = cDeportista.isChecked();
                i.putExtra("athlete", deportista);

                CheckBox cAlcoholico = (CheckBox) findViewById(R.id.idAlcoholico);
                boolean alcoholico = cAlcoholico.isChecked();
                i.putExtra("alcoholic", alcoholico);

                CheckBox cGamer = (CheckBox) findViewById(R.id.idGamer);
                boolean gamer = cGamer.isChecked();
                i.putExtra("gamer", gamer);

                startActivity(i);

            } else if(control.editTextIsNullOrEmpty(textNom)){
                Toast.makeText(getApplicationContext(),"Introduce el nombre",Toast.LENGTH_SHORT).show();
            } else if(control.editTextIsNullOrEmpty(textLugar)){
                Toast.makeText(getApplicationContext(),"Introduce el lugar de nacimiento",Toast.LENGTH_SHORT).show();

            }









        }
    }
}