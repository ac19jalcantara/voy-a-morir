package com.example.myapplication.mains;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.control.control;


public class MainActivity2_muerte extends AppCompatActivity {

        private TextView cabecera;
        private TextView cuerpo;
        private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2_muerte);



        cabecera=findViewById(R.id.idTx);
        cuerpo=findViewById(R.id.textView);
        img=findViewById(R.id.idImg);
        img.setImageResource(R.drawable.lapida);


        pesame();


    }




    public void pesame()
    {
        String nom = getIntent().getStringExtra("name");

        String sexo = getIntent().getStringExtra("gender");


        String fecha = getIntent().getStringExtra("date");

        String lugar = getIntent().getStringExtra("place");

        boolean fumador = getIntent().getBooleanExtra("smoker",true);

        boolean deportista = getIntent().getBooleanExtra("athlete", false);

        boolean alcoholico = getIntent().getBooleanExtra("alcoholic", false);

        boolean gamer = getIntent().getBooleanExtra("gamer", false);

        cabecera.setText(control.mortCapçelera(nom,sexo,lugar,fecha));


        cuerpo.setText(control.mortCos(sexo,fumador,deportista,gamer,alcoholico));




    }


}